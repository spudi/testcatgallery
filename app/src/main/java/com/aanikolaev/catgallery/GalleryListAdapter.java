package com.aanikolaev.catgallery;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aanikolaev.catgallery.loader.ImageLoader;

import java.util.ArrayList;

public class GalleryListAdapter extends RecyclerView.Adapter<GalleryListAdapter.ViewHolder> {

    private ArrayList<String> items;
    private GalleryOnItemClickLister onItemClickListener;

    private ImageLoader imageLoader;

    public GalleryListAdapter() {
        imageLoader = new ImageLoader()
                .setPlaceholder(R.drawable.no_image);
    }

    public void setItems(ArrayList<String> items) {
        if (this.items != null) {
            this.items.clear();
            this.items.addAll(items);
        } else {
            this.items = new ArrayList<>(items);
        }
        notifyDataSetChanged();
    }

    public void addItemToStart(String item) {
        if (this.items == null) {
            this.items = new ArrayList<>();
        }
        this.items.add(0, item);
        notifyItemInserted(items.size() - 1);
    }

    public void setOnItemClickListener(final GalleryOnItemClickLister GalleryOnItemClickLister) {
        this.onItemClickListener = GalleryOnItemClickLister;
    }

    @Override
    public GalleryListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_gallery, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        imageLoader.load(holder.photoPreview, getItem(position));
    }

    public String getItem(int position) {
        return items != null && position < items.size() ? items.get(position) : null;
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView photoPreview;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            photoPreview = (ImageView) itemView.findViewById(R.id.itemGallery_ivImage);
        }

        @Override
        public void onClick(View view) {
            if (onItemClickListener != null) {
                onItemClickListener.onItemClicked(getItem(getAdapterPosition()), getAdapterPosition());
            }
        }
    }

    public interface GalleryOnItemClickLister {
        void onItemClicked(String item, int position);
    }
}
