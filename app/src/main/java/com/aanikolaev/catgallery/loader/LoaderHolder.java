package com.aanikolaev.catgallery.loader;

import android.widget.ImageView;

/**
 * Created by Roman Shylo on 04.04.2017.
 * whoose.daddy@gmail.com
 */

public class LoaderHolder {

    private ImageView target;
    private BitmapWorkerTask task;

    LoaderHolder(ImageView ivTarget) {
        this.target = ivTarget;
    }

    void setPlaceholder(int resId) {
        target.setImageBitmap(null);
        target.setImageResource(resId);
    }

    void loadImage(String path) {
        cancelTask();
        task = new BitmapWorkerTask(target);
        task.execute(path);
    }

    void cancelTask() {
        if (task != null) {
            task.cancel(true);
            task = null;
        }
    }

}