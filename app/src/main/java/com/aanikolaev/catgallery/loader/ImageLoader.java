package com.aanikolaev.catgallery.loader;

import android.support.annotation.DrawableRes;
import android.util.Log;
import android.widget.ImageView;

import java.util.HashMap;

/**
 * Created by Roman Shylo on 04.04.2017.
 * whoose.daddy@gmail.com
 */

public class ImageLoader {

    private static final String TAG = ImageLoader.class.getSimpleName();

    private static final int UNDEFINED_PLACEHOLDER = -1;

    private HashMap<ImageView, LoaderHolder> loaderHolders;
    private @DrawableRes int placeholder = UNDEFINED_PLACEHOLDER;

    public ImageLoader() {
        loaderHolders = new HashMap<>();
    }

    public ImageLoader setPlaceholder(int placeholder) {
        this.placeholder = placeholder;
        return this;
    }

    public void load(ImageView imageView, String path) {
        LoaderHolder holder;
        if (loaderHolders.containsKey(imageView)) {
            // Recycled image view
            Log.i(TAG, "load -> Recycled image view");
            holder = loaderHolders.get(imageView);
        } else {
            // New image view
            Log.i(TAG, "load -> New image view");
            holder = new LoaderHolder(imageView);
            loaderHolders.put(imageView, holder);
        }
        makeLoad(holder, path);
    }

    private void makeLoad(LoaderHolder holder, String path) {
        if (UNDEFINED_PLACEHOLDER != placeholder) {
            // Set placeholder if needed
            holder.setPlaceholder(placeholder);
        }
        holder.loadImage(path);
    }

}