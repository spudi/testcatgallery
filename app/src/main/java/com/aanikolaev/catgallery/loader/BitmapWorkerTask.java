package com.aanikolaev.catgallery.loader;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.aanikolaev.catgallery.utils.Constant;

import java.io.File;
import java.lang.ref.WeakReference;

/**
 * Created by laffy on 31.03.2017.
 */

public class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {

    private final WeakReference<ImageView> imageViewReference;

    public BitmapWorkerTask(ImageView imageView) {
        imageViewReference = new WeakReference<>(imageView);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        String path = params[0];
        File file = new File(path);
        if (file.exists()) {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), bmOptions);

            return Bitmap.createScaledBitmap
                    (bitmap, Constant.lHIGHT_PHOTO, Constant.lWIGHT_PHOTO, false);
        } else {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (bitmap != null) {
            final ImageView imageView = imageViewReference.get();
            if (imageView != null) {
                imageView.setImageBitmap(bitmap);

            }
        }
    }
}