package com.aanikolaev.catgallery;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.aanikolaev.catgallery.utils.Constant;
import com.bumptech.glide.Glide;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private ImageView imagePreview;
    private RecyclerView recyclerViewGallery;

    private String photoPath;
    private Uri directory;

    private GalleryListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configWindow();
        setContentView(R.layout.activity_main);
        findViews();
        initRecycler();
        loadGallery(items -> {
            if (items != null && !items.isEmpty()) {
                adapter.setItems(items);
            } else {
                Toast.makeText(MainActivity.this, "No data to displaying", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        photoPath = savedInstanceState.getString(Constant.MAIL);
        setImage(photoPath);
        directory = savedInstanceState.getParcelable("outputFileUri");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Constant.MAIL, photoPath);
        outState.putParcelable("outputFileUri", directory);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constant.CAMERA_REQUEST && resultCode == RESULT_OK) {
            // FIXME
            /*
              Uri object IT'S NOT A STRING
              directory.toString() -> returns: file:///storage/emulated/0/Pictures/CatGallery/photo_20170404_235628.jpg
              directory.getPath() -> returns: storage/emulated/0/Pictures/CatGallery/photo_20170404_235628.jpg
              onNewImageAdded(directory.toString());
             */
            // Small delay for smooth scrolling on recycler
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onNewImageAdded(directory.getPath());
                }
            }, 500);
        }
    }

    private void configWindow() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    private void findViews() {
        imagePreview = (ImageView) findViewById(R.id.photoPreview);
        recyclerViewGallery = (RecyclerView) findViewById(R.id.recyclerViewGallery);

        findViewById(R.id.btnShot).setOnClickListener((v) -> onClickTakePhoto());
    }

    private void initRecycler() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        boolean useVertical = getResources().getConfiguration().orientation != Configuration.ORIENTATION_PORTRAIT;
        layoutManager.setOrientation(useVertical ? LinearLayoutManager.VERTICAL : LinearLayoutManager.HORIZONTAL);
        recyclerViewGallery.setLayoutManager(layoutManager);
        // Adapter
        adapter = new GalleryListAdapter();
        adapter.setOnItemClickListener(this::onGalleryItemClicked);
        recyclerViewGallery.setAdapter(adapter);
    }

    private void setImage(String path) {
        // FIXME
        /*
         * don't use global app context to init Glide, use activity's context instead
         * Glide.with(getApplicationContext())
         */
        Glide.with(this)
                .load(path)
//                .override(Constant.pHIGHT_PHOTO, Constant.pWIGHT_PHOTO)
                .centerCrop()
                .into(imagePreview);
//        new BitmapWorkerTask(imagePreview).execute(path);
    }

    private void onNewImageAdded(String path) {
        Log.i(TAG, "onNewImageAdded: " + path);
        this.photoPath = path;
        setImage(path);
        adapter.addItemToStart(path);
        recyclerViewGallery.smoothScrollToPosition(0);
    }

    private void onClickTakePhoto() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        directory = generateFileUri();
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, directory);
        startActivityForResult(cameraIntent, Constant.CAMERA_REQUEST);
    }

    private Uri generateFileUri() {
        File folder = new File(
                Environment.getExternalStoragePublicDirectory
                        (Environment.DIRECTORY_PICTURES), Constant.NAME_FOLDER);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        File file = new File(folder.getPath() + "/" + "photo_"
                + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US)
                .format(new Date()) + ".jpg");
        return Uri.fromFile(file);
    }

    private void onGalleryItemClicked(String item, int position) {
        setImage(item);
        photoPath = item;
    }

    private void loadGallery(OnGalleryLoadCallback callback) {
        File folder = new File(Environment.getExternalStoragePublicDirectory
                (Environment.DIRECTORY_PICTURES), Constant.NAME_FOLDER);
        ArrayList<String> photos = null;
        // FIXME
        /*
         * folder.listFiles() can be NOT null, but EMPTY (.length == 0)
         * check for empty array
         * folder.listFiles().length > 0
         * without this check you get IndexOfBoundException on setImage(photos.get(0)); line
         */
        if (folder.listFiles() != null && folder.listFiles().length > 0) {
            photos = new ArrayList<>();
            for (File file : folder.listFiles()) {
                if (file.isFile()) {
                    photos.add(0, file.getPath());
                }
            }
            setImage(photos.get(0));
        } else {
            imagePreview.setImageResource(R.drawable.no_image);
        }
        callback.onFilesLoaded(photos);
    }

    public interface OnGalleryLoadCallback {
        void onFilesLoaded(@Nullable ArrayList<String> items);
    }

}