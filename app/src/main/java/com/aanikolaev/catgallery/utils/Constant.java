package com.aanikolaev.catgallery.utils;


public class Constant {

    public static final int ORIENTATION = 0;
    public static final int CAMERA_REQUEST = 1;
    public static final int pHIGHT_PHOTO = 800;
    public static final int pWIGHT_PHOTO = 800;
    public static final int lHIGHT_PHOTO = 100;
    public static final int lWIGHT_PHOTO = 100;
    public static final String MAIL = "photo";
    public static final String NAME_FOLDER = "CatGallery";
}
